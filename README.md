[![pipeline status](https://gitlab.com/rsihammou/blog/badges/master/pipeline.svg)](https://gitlab.com/rsihammou/blog/commits/master)

# Based-Pelican static blog on gitlab pages
A simple blog created with Pelican as a static site generator, we used also gitlab-ci to automate build and deploy steps.

The genrated files will be pushed to gitlab pages.

## Install Pelican

You start by cloning projet from gitlab
```
git clone https://gitlab.com/rsihammou/blog.git
```


To work properly, you can create your working environement with virtualenv
```
$ sudo apt install virtualenv 
$ virtualenv venv
$ source venv/bin/activate
```
Pelican allows to write content in Markdown laguage and build pretty good HTML.

Pelican is written in Python, so you can use pip to install it.
```
$ pip intall pelican Markdown
```

## Write a content and start blog locally

This blog example has been initialized by Pelican using `pelican-quickstart`, this cammand asks a set of questions and creates a skeleton project.
```
$ pelican-quickstart
```
```
Welcome to pelican-quickstart v3.7.1.

This script will help you create a new Pelican-based website.

Please answer the following questions so this script can generate the files
needed by Pelican.

> Where do you want to create your new web site? [.] .
> What will be the title of this web site? My blog
> Who will be the author of this web site? My name
> What will be the default language of this web site? [en] en
> Do you want to specify a URL prefix? e.g., http://example.com   (Y/n) y
> What is your URL prefix? (see above example; no trailing slash) http://<github-account>.github.io/blog
> Do you want to enable article pagination? (Y/n) y
> How many articles per page do you want? [10] 5
> What is your time zone? [Europe/Paris] 
> Do you want to generate a Fabfile/Makefile to automate generation and publishing? (Y/n) y
> Do you want an auto-reload & simpleHTTP script to assist with theme and site development? (Y/n) y
> Do you want to upload your website using FTP? (y/N) n
> Do you want to upload your website using SSH? (y/N) n
> Do you want to upload your website using Dropbox? (y/N) n
> Do you want to upload your website using S3? (y/N) n
> Do you want to upload your website using Rackspace Cloud Files? (y/N) n
> Do you want to upload your website using GitHub Pages? (y/N) y
> Is this your personal page (username.github.io)? (y/N) n
Done. Your new project is available at /path/to/the/blog/
```
It is convenient to run development server on [https://localhost:8000](https://localhost:8000) that will display changes as you make them. 
```
$ make devserver
```
All written content should be stored in the content directory and should have metadata like:
```
Title: My title
Date: 2020-07-24 10:20
Modified: 2020-07-25 19:30
Category: Python
Tags: pelican, blog
Authors: Rabie Sihammou
Summary: Some summary

A simple content of my blog post.
```
`pelican-quickstart` creates two configuration files:

* pelicanconf.py for local development, this file contains some parameters definded previously as author, sitename, lang ...
* publishconf.py for product deployment

By default Pelican generates html files in the output directory, you need to rename it to pubic for pages step of gitlab-ci.
Add OUTPUT_PATH parameter to the pelicanconf.py
```
OUTPUT_PATH = 'public/'
```
Also redefine 'OUTPUTDIR' parameter in the Makefile script
```
OUTPUTDIR=$(BASEDIR)/public
```

## Gitlab-ci

With gitlab-ci you can quickly create your Continuous Integration (CI) and Continuous Delivery (CD).
Our .gitlab-ci.yml file contains tow jobs 

```
stages:
  - build
  - deploy
```

1. Build job:

The buid job is automaticly executed when you push changements.
```
image: python:2.7-alpine
build:
  stage: build
  script:
    - apk update && apk add make
    - pip install pelican markdown
    - make html
```
The script update python-based image, install Pelican and Markdowd
finally genrate html pages in the public directory

2. Deploy job:

For more security, it is convenient to execute manually deploy job.

If you want to use GitLab Pages website, you should define job called pages in the gitlab-ci configuration file.

GitLab always deploys your website from a very specific folder called public in your repository.
```
pages:
  stage: deploy
  script:
    - apk update && apk add make
    - pip install pelican markdown
    - make publish
  artifacts:
    paths:
      - public
  only:
   - master
  when: manual
```

## Access to Pages site

The simple way to acces to your Pages site is to use GitLab Pages default domain (.gitlab.io), the website is automatically secure and available under HTTPS.
The website url will be `https://<GitLab_username>.gittlab.io/<projectname>`

Our url blog site is: 
[https://rsihammou.gitlab.io/blog/](https://rsihammou.gitlab.io/blog)

  
