Title: Hello summer 2020
Date: 2020-07-19 21:42
Tags: Accueil, Concerts, Spectacles, Expositions
Category: Accueil
Authors: Rabie sihammou
Summary: Site dédiée aux événements culturels de l'été 2020.


# Que faire à Paris en juillet et août 2021


En juillet et en août, Paris fête l’été en proposant **de nombreux événements en plein air**. Il fait beau et le soleil est à son zénith ! Pour profiter au mieux des longues journées et des douces soirées, les possibilités sont multiples. Au hit-parade d’un été parisien réussi, faites la fête le **14 Juillet**, relaxez-vous à **Paris Plages**, vibrez pour l’arrivée du **Tour de France** et profitez de tous les festivals de la capitale !

Petite sélection des **incontournables de la saison**.

## 14 juillet à Paris 
 
![HELLO SUMMER 2020](./images/Patrouille-de-France-Seine-14-juillet.jpg).

Le temps fort de l’été, c’est la **fête nationale** qui a lieu le 14 juillet.

>Cette année pas de défilé militaire et un feu d’artifice à huis-clos à découvrir uniquement à la télévision et depuis vos balcons mais un véritable show dans le ciel avec un **spectacle aérien** qui va vous en mettre plein les yeux. Suivez la plus longue rue parisienne, la **rue de Rivoli** et vous serez au premier plan pour voir défiler une centaine d’avions et hélicoptères dès 10h30.

>Pour passer un 14 juillet mémorable, une série d'événements sont proposés tout au long de cette journée. Concerts, gratuités, spectacles, c'est à ne pas manquer !

## Paris Plages

![HELLO SUMMER 2020](./images/Grands-événements-juillet-aout-Paris-Plages.jpg).

Durant l’été, Paris se transforme en station balnéaire et accueille le rendez-vous Paris Plages (18 juillet - 30 août 2020). 
>Dans le Parc Rives de Seine, les transats et les palmiers investissent les quais afin de pouvoir se détendre comme sur une plage, **au bord de l'eau**. De **nombreuses activités** sont proposées aux grands comme aux petits dans le respect de la distanciation sociale et gestes barrières.

## Concerts et spectacles

![HELLO SUMMER 2020](./images/Spectacle-Marie-Antoinette-Domaine-de-Versailles.jpg).

En été, les mélomanes sont gâtés. En plus des nombreux festivals de musique qui font résonner la capitale, il existe une riche programmation estivale de concerts et de spectacles.

>Dans **les jardins** du château de Versailles, **Marie-Antoinette** est à l’honneur avec un **spectacle pyrotechnique** époustouflant à découvrir en famille à la tombée de la nuit (du 2 au 10 juillet 2020) tandis que les Grandes Eaux Nocturnes sont de retour dès le 27 juin jusqu’au 19 septembre et les Grandes Eaux Musicales jusqu’au 1er novembre.
